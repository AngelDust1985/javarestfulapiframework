package Servlet;

import org.eclipse.jetty.http.HttpStatus;

import javax.servlet.http.HttpServlet;
import java.io.IOException;

import javax.servlet.ServletException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

///**
// * Created by vadim on 5/28/17.
// */
public class Goods  extends HttpServlet
{


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        resp.setStatus(HttpStatus.OK_200);
        resp.getWriter().println("Goods");
    }

}
