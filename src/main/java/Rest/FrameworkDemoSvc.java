package Rest;

import DAL.DalFactory;
import DAL.MongoDbBox;
import Exceptions.FrameworkCatchManager;
import Exceptions.ServiceException;
import Interfaces.IDal;
import Model.*;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import org.eclipse.jetty.server.Response;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import java.util.Date;

/**
 * Created by vadim on 6/16/17.
 */


@Path("/FrameworkDemo")
public class FrameworkDemoSvc {

    @GET
    @Path("/getDemo")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseBase<String> getDemo() {
        ResponseBase<String> oResponse = new ResponseBase<String>();

        oResponse.payload = "Demo";
        oResponse.sucess = true;
        return oResponse;

    }


    @POST
    @Path("/postDemo")
    public ResponseBase<Boolean> postEmployee(Employee employee) {
        ResponseBase<Boolean> oResponse = new ResponseBase<Boolean>();
        try {

            oResponse.payload = true;
            DalFactory<Employee> factoryDal = new DalFactory<Employee>();
            IDal<Employee> box  = factoryDal.GetCollectionBox(Enumerations.DalCollectionsEnum.Mongo);
            box.Insert(employee,"Employee");

        } catch (Exception e) {

            FrameworkCatchManager.LogicCatchFunc(oResponse, new ServiceException("postEmployee"));

        }
        return oResponse;
    }

    @POST
    @Path("/postDemoObject")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseBase<Employee> postDemoObject() {
        ResponseBase<Employee> oResponse = new ResponseBase<Employee>();
        try {

            oResponse.payload = Employee.getRandomEmployee();
            DalFactory<Employee> factoryDal = new DalFactory<Employee>();
            IDal<Employee> box  = factoryDal.GetCollectionBox(Enumerations.DalCollectionsEnum.Mongo);
            box.Insert(oResponse.payload,"Employee");

        } catch (Exception e) {

            FrameworkCatchManager.LogicCatchFunc(oResponse, new ServiceException("postEmployee"));

        }
        return oResponse;
    }

}

