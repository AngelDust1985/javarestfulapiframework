package Rest;

import DAL.DalFactory;
import DAL.MongoDbBox;
import Interfaces.IDal;
import Model.Address;
import Model.Employee;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Date;

/**
 * Created by vadim on 6/12/17.
 */
@Path("/employee")
public class EmployeeSvc {


    // static int a = 0;

    @GET
    @Path("/getEmployee")
    @Produces(MediaType.APPLICATION_JSON)

    public Employee getEmployee() {


        return Employee.getRandomEmployee();
//        Employee employee = new Employee();
//        employee.setName("John");
//        employee.setAge(40);
//        employee.setDeparment("HR");
//        employee.setWage(15000.00);
//        Address address = new Address();
//        address.setCity("Massachusetts");
//        address.setState("Springfield");
//        address.setStreet("Evergreen");
//        address.setZip(66450);
//        employee.setAddress(address);
        // return employee;
    }


    @GET
    @Path("/Action")
    @Produces(MediaType.APPLICATION_JSON)

    public String Action() {

        return "{\"vadim\":\"Vadim\"}";
    }

    @POST
    @Path("/postEmployee")
    public void postEmployee(Employee employee) {
        try {
            MongoDbBox db = new MongoDbBox();

            System.out.println("Output json server .... \n");
            System.out.println(employee);

        } catch (Exception e) {


        }
    }


    @POST
    @Path("/updateEmployee")
    public void updateEmployee(Employee employee) {
        try {

            System.out.println("Output json server .... \n");
            System.out.println(employee);


        } catch (Exception e) {


        }


    }


}
