package DAL;

import Exceptions.FrameworkCatchManager;
import Exceptions.ServiceException;
import Interfaces.IDal;
import Model.Employee;
import com.google.gson.Gson;
import com.mongodb.*;
import com.mongodb.util.JSON;

import java.util.List;

/**
 * Created by vadim on 6/13/17.
 */
public class MongoDbBox<T> implements IDal<T> {


    private static volatile MongoClient mongo;
    private static final Object lock = new Object();


    public MongoDbBox() {
        try {

        } catch (Exception e) {

            System.out.println("Error json server .... \n");
            System.out.println(e);
        }
    }

    public MongoClient getMongo() {

        MongoClient r = mongo;
        if (r == null) {
            synchronized (lock) {
                r = mongo;
                if (r == null) {
                    r = new MongoClient();
                    mongo = r;
                }
            }
        }
        return r;
    }


    public void Insert(T obj, String dbTableName) {

        try {


            DBCollection table = this.getMongoDb("MongoTest").getCollection(dbTableName);
            BasicDBObject document = new BasicDBObject();
            obj.getClass();
            Gson gson = new Gson();
            String json = gson.toJson(obj);
            DBObject dbObject = (DBObject) JSON.parse(json);
            table.insert(dbObject);


        } catch (Exception e) {

            FrameworkCatchManager.LogicCatchFunc(null, new ServiceException("Mongo Insert"));

        }

    }

    public void Update(T obj, String dbTableName) {


    }


    public void Delete(T obj, String dbTableName) {


    }


    public List<T> Read(String dbTableName) {


        return null;
    }

    public DB getMongoDb(String db) {


        return getMongo().getDB(db);

    }


}
