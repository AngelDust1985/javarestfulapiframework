/**
 * Created by vadim on 5/28/17.
 */

import Servlet.*;

import com.sun.jersey.spi.container.servlet.ServletContainer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;


public class EmbeddedJettyMain {


    public static void main(String[] args) throws Exception {


        Server server = new Server(7070);
        ServletContextHandler handler = new ServletContextHandler(server, "/Example",ServletContextHandler.SESSIONS);


        ServletHolder h = new ServletHolder(new ServletContainer());
        h.setInitParameter("com.sun.jersey.config.property.packages", "Rest");
        h.setInitOrder(0);


       handler.addServlet(Goods.class, "/Goods");
       handler.addServlet(Users.class, "/Users");
       handler.addServlet(h, "/Jersey/*");

        try {
            server.start();
            server.join();
        } catch(Exception e) {
            server.destroy();
        }



    }

}
