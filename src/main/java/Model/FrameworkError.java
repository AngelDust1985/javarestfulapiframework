package Model;

import Exceptions.BaseException;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by vadim on 6/16/17.
 */
@XmlRootElement
public class FrameworkError  {


    public String message;
    public int id = -1;

    public FrameworkError() {

        this.message = "";
        this.id = -1;

    }

    public FrameworkError(String message, int id) {
        this.message = message;
        this.id = id;
    }


}
