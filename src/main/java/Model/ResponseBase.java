package Model;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.io.Serializable;

/**
 * Created by vadim on 6/16/17.
 */
@XmlRootElement
@Provider
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@XmlSeeAlso(Employee.class)
public class ResponseBase<T> implements Serializable, ContextResolver<JAXBContext> {

  private static final long serialVersionUID = 1L;
  public FrameworkError error;
  public T payload;
  public Boolean sucess;
  public enum MessageCode {
    SUCCESS, ERROR, UNKNOWN
  }

  private JAXBContext ctx;



  public ResponseBase() {
    try {
    this.error = new FrameworkError("",-1);
    this.sucess = true;
    this.ctx = JAXBContext.newInstance(
              Model.Employee.class,
              Student.class,
              Address.class
      );
    } catch (JAXBException ex) {
      throw new RuntimeException(ex);
    }

  }


  public JAXBContext getContext(Class<?> type) {
    return (type.equals(ResponseBase.class) ? ctx : null);
  }

}
