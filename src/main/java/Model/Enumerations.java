package Model;

/**
 * Created by vadim on 6/18/17.
 */
public class Enumerations {

   public enum ErrorExceptions {

       ServerException(1), ServiceException(2),GeneralException(3),DalException(4);


       private final int id;
       ErrorExceptions(int id) { this.id = id; }
       public int getValue() { return id; }


   }
   public enum DalCollectionsEnum { Mongo,MySQL }

}
