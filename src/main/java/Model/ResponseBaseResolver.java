package Model;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

/**
 * Created by vadim on 6/16/17.
 */


@Provider
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public class ResponseBaseResolver implements ContextResolver<JAXBContext>{


        private JAXBContext ctx;

        public ResponseBaseResolver() {
            try {
                this.ctx = JAXBContext.newInstance(
                            Employee.class
                      //  Response.class,
                       // Department.class

                );
            } catch (JAXBException ex) {
                throw new RuntimeException(ex);
            }
        }

        public JAXBContext getContext(Class<?> type) {
            return (type.equals(ResponseBase.class) ? ctx : null);
        }
    }

