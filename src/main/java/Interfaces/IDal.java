package Interfaces;

import java.util.List;

/**
 * Created by vadim on 6/14/17.
 */
public interface IDal<T> {

    void Insert(T obj,String dbTableName);
    void Update(T obj,String dbTableName);
    void Delete(T obj,String dbTableName);
    List<T> Read(String dbTableName);


}
