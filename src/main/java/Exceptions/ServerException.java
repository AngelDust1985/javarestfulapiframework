package Exceptions;

import Model.Enumerations;
import Model.FrameworkError;
import Model.ResponseBase;
import com.fasterxml.jackson.databind.ser.Serializers;

/**
 * Created by vadim on 6/15/17.
 */
public class ServerException extends BaseException {


    private String message;
    private int id = Enumerations.ErrorExceptions.ServerException.getValue();

    public ServerException(String message)
    {
        super(message);
    }


    public ServerException(Exception ex)
    {
        super(ex);
    }


    public ServerException(String message,Exception ex)
    {
        super(message,ex);
    }

    //TO DO IMPLEMENT LOGIC FUNCTION FOR LOGGING LOG4J
    public void LogicCatch(ResponseBase<?> oRes){
        oRes.sucess = false;
        oRes.error = new FrameworkError(this.message,this.id);
    }
}
