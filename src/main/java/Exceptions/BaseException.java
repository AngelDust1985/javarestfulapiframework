package Exceptions;

import Model.ResponseBase;

/**
 * Created by vadim on 6/15/17.
 */
public abstract class BaseException extends Exception {


    public BaseException() {
    }

    public BaseException(String message) {
        super(message);
    }

    public BaseException(Exception ex) {
        super(ex);
    }

    public BaseException(String message, Exception exp) {
        super(message, exp);
    }

    public abstract void LogicCatch(ResponseBase<?> oRes);
}
