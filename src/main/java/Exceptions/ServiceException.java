package Exceptions;

import Model.Enumerations;
import Model.FrameworkError;
import Model.ResponseBase;

/**
 * Created by vadim on 6/14/17.
 */
public class ServiceException extends BaseException {
    private String message;
    private int id = Enumerations.ErrorExceptions.ServiceException.getValue();

    public ServiceException(String message) {
        super(message);

    }

    public ServiceException(Exception ex) {
        super(ex);

    }


    public ServiceException(String message, Exception ex) {
        super(message, ex);

    }


    public ServiceException(String message, int id) {
        super(message);

        this.id = id;
    }

    //TO DO IMPLEMENT LOGIC FUNCTION FOR LOGGING LOG4J
    public void LogicCatch(ResponseBase<?> oRes) {
        oRes.sucess = false;
        oRes.error = new FrameworkError(this.message, this.id);

    }
}
