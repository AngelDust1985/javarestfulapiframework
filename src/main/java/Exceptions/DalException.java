package Exceptions;

import Model.Enumerations;
import Model.FrameworkError;
import Model.ResponseBase;

/**
 * Created by vadim on 6/20/17.
 */
public class DalException extends BaseException{

    private String message;
    private int id = Enumerations.ErrorExceptions.DalException.getValue();

    public DalException(String message)
    {
        super(message);
    }

    public DalException(Exception ex)
    {
        super(ex);
    }

    public DalException(String message,Exception ex)
    {
        super(message,ex);
    }

    //TO DO IMPLEMENT LOGIC FUNCTION FOR LOGGING LOG4J
    public void LogicCatch(ResponseBase<?> oRes){
        oRes.sucess = false;
        oRes.error = new FrameworkError(this.message,this.id);

    }

}
