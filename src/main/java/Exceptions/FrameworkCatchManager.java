package Exceptions;

import Model.Enumerations;
import Model.FrameworkError;
import Model.ResponseBase;

/**
 * Created by vadim on 6/18/17.
 */
public class FrameworkCatchManager {


    public static void LogicCatchFunc(ResponseBase<?> oRes, Exception exp)
    {
        if (exp instanceof BaseException)
        {
            ((BaseException)exp).LogicCatch(oRes);
        }
            else
        {
            oRes.sucess = false;
            oRes.error = new FrameworkError(exp.getMessage(),Enumerations.ErrorExceptions.GeneralException.getValue());
        }

    }



}
